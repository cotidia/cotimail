Documentation
=============

[Installation](https://gitlab.com/cotidia/cotimail/wikis/Installation)

[Settings reference](https://gitlab.com/cotidia/cotimail/wikis/settings-reference)

[Admin](https://gitlab.com/cotidia/cotimail/wikis/admin)

[Usage](https://gitlab.com/cotidia/cotimail/wikis/usage)

[Notice reference](https://gitlab.com/cotidia/cotimail/wikis/notice-reference)

[Commands](https://gitlab.com/cotidia/cotimail/wikis/commands)

[Dependencies](https://gitlab.com/cotidia/cotimail/wikis/dependencies)

Run tests:

	$ python manage.py test cotimail.tests --keepdb